# Assignment Three

This is a base program for an RPG game. It contains the possibility to create characters, weapons and armors. You can equip your character with these weapons and armors, and deal damage with your weapon.