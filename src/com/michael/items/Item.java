package com.michael.items;

public abstract class Item {
    String name;
    int reqLevel;

    public Item(String name, int reqLevel) {
        this.name = name;
        this.reqLevel = reqLevel;
    }

    public String getName() {
        return name;
    }

    public int getReqLevel() {
        return reqLevel;
    }
}
