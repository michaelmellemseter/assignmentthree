package com.michael.items.armors;

import com.michael.heroes.Hero;
import com.michael.items.Item;
import com.michael.items.armors.equipArmor.EquipArmorStrategy;

public abstract class Armor extends Item {
    int slot;  //1 = head, 2 = body, 3 = legs
    EquipArmorStrategy equipArmorStrategy;

    public Armor(String name, int reqLevel, int slot, EquipArmorStrategy equipArmorStrategy) {
        super(name, reqLevel);
        this.slot = slot;
        this.equipArmorStrategy = equipArmorStrategy;
    }

    public void equipArmor(Hero hero) {
        equipArmorStrategy.equipArmor(this, hero);
    }

    public int getSlot() {
        return slot;
    }
}
