package com.michael.items.armors.equipArmor;

import com.michael.heroes.Hero;
import com.michael.items.armors.Armor;

public class EquipClothArmor implements EquipArmorStrategy {
    @Override
    public void equipArmor(Armor armor, Hero hero) {
        if (hero.getLevel() >= armor.getReqLevel()) {  //checking if the hero has a high enough level
            if (armor.getSlot() == 1) {  //head
                hero.setHead(armor);
                //setting all the extra head stats
                hero.setExtraHeadHealth((int) Math.floor(10 + 5 * armor.getReqLevel() * 0.8));
                hero.setExtraHeadIntelligence((int) Math.floor(3 + 2 * armor.getReqLevel() * 0.8));
                hero.setExtraHeadDexterity((int) Math.floor(1 + armor.getReqLevel() * 0.8));
                hero.setExtraHeadStrength(0);
                System.out.println(armor.getName() + " is equipped");
            } else if (armor.getSlot() == 2 ) {  //body
                hero.setBody(armor);
                //setting all the extra body stats
                hero.setExtraBodyHealth(10 + 5 * armor.getReqLevel());
                hero.setExtraBodyIntelligence(3 + 2 * armor.getReqLevel());
                hero.setExtraBodyDexterity(1 + armor.getReqLevel());
                hero.setExtraBodyStrength(0);
                System.out.println(armor.getName() + " is equipped");
            } else if (armor.getSlot() == 3) {  //legs
                hero.setLegs(armor);
                //setting all the extra legs stats
                hero.setExtraLegsHealth((int) Math.floor(10 + 5 * armor.getReqLevel() * 0.6));
                hero.setExtraLegsIntelligence((int) Math.floor(3 + 2 * armor.getReqLevel() * 0.6));
                hero.setExtraLegsDexterity((int) Math.floor(1 + armor.getReqLevel() * 0.6));
                hero.setExtraLegsStrength(0);
                System.out.println(armor.getName() + " is equipped");
            }
        } else {
            System.out.println("You don't have high enough level to equip this armor");
        }
    }
}
