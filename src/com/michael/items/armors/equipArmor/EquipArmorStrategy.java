package com.michael.items.armors.equipArmor;

import com.michael.heroes.Hero;
import com.michael.items.armors.Armor;

public interface EquipArmorStrategy {
    void equipArmor(Armor armor, Hero hero);
}
