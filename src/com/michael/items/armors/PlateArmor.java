package com.michael.items.armors;

import com.michael.items.armors.equipArmor.EquipArmorStrategy;

public class PlateArmor extends Armor {
    public PlateArmor(String name, int reqLevel, int slot, EquipArmorStrategy equipArmorStrategy) {
        super(name, reqLevel, slot, equipArmorStrategy);
    }
}
