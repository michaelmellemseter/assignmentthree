package com.michael.items.weapons.dealDamage;

import com.michael.heroes.Hero;

public interface WeaponDamageStrategy {
    void dealDamage(Hero hero);
}
