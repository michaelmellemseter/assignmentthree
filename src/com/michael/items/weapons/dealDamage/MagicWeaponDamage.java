package com.michael.items.weapons.dealDamage;

import com.michael.heroes.Hero;

public class MagicWeaponDamage implements WeaponDamageStrategy {
    @Override
    public void dealDamage(Hero hero) {  //calculating the damage dealt by the hero
        System.out.println("Damage dealt: " + ((int) Math.floor(hero.getEffectiveIntelligence() * 3) + hero.getWeapon().getBaseDamage() + 2 * hero.getWeapon().getReqLevel()));
    }
}
