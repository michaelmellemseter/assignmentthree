package com.michael.items.weapons.dealDamage;

import com.michael.heroes.Hero;

public class MeleeWeaponDamage implements WeaponDamageStrategy{
    @Override
    public void dealDamage(Hero hero) {  //calculating the damage dealt by the hero
        System.out.println("Damage dealt: " + ((int) Math.floor(hero.getEffectiveStrength() * 1.5 + hero.getWeapon().getBaseDamage() + 2 * hero.getWeapon().getReqLevel())));
    }
}
