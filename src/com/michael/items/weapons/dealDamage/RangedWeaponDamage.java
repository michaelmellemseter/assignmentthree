package com.michael.items.weapons.dealDamage;

import com.michael.heroes.Hero;

public class RangedWeaponDamage implements WeaponDamageStrategy {
    @Override
    public void dealDamage(Hero hero) {  //calculating the damage dealt by the hero
        System.out.println("Damage dealt: " + ((int) Math.floor(hero.getEffectiveDexterity() * 2) + hero.getWeapon().getBaseDamage() + 3 * hero.getWeapon().getReqLevel()));
    }
}
