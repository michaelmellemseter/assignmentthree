package com.michael.items.weapons;

import com.michael.items.weapons.dealDamage.WeaponDamageStrategy;

public class MeleeWeapon extends Weapon {
    int baseDamage;

    public MeleeWeapon(String name, int reqLevel, WeaponDamageStrategy weaponDamageStrategy) {
        super(name, reqLevel, weaponDamageStrategy);
        this.baseDamage = 15;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;
    }
}
