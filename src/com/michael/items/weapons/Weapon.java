package com.michael.items.weapons;

import com.michael.heroes.Hero;
import com.michael.items.Item;
import com.michael.items.weapons.dealDamage.WeaponDamageStrategy;

public abstract class Weapon extends Item {
    WeaponDamageStrategy weaponDamageStrategy;

    public Weapon(String name, int reqLevel, WeaponDamageStrategy weaponDamageStrategy) {
        super(name, reqLevel);
        this.weaponDamageStrategy = weaponDamageStrategy;
    }

    public void dealDamage(Hero hero) {
        weaponDamageStrategy.dealDamage(hero);
    }

    public abstract int getBaseDamage();
}
