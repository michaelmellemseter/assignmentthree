package com.michael.items.weapons;

import com.michael.items.weapons.dealDamage.WeaponDamageStrategy;

public class RangedWeapon extends Weapon {
    int baseDamage;

    public RangedWeapon(String name, int reqLevel, WeaponDamageStrategy weaponDamageStrategy) {
        super(name, reqLevel, weaponDamageStrategy);
        this.baseDamage = 5;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;
    }
}
