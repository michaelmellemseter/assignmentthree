package com.michael.items.weapons;

import com.michael.items.weapons.dealDamage.WeaponDamageStrategy;

public class MagicWeapon extends Weapon {
    int baseDamage;

    public MagicWeapon(String name, int reqLevel, WeaponDamageStrategy weaponDamageStrategy) {
        super(name, reqLevel, weaponDamageStrategy);
        this.baseDamage = 25;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;
    }
}
