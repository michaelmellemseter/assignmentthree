package com.michael.heroes;

public class Warrior extends Hero {
    int health;
    int strength;
    int dexterity;
    int intelligence;


    public Warrior(String name) {  //setting values for attributes to a warrior
        super(name);
        this.health = 150;
        this.strength = 10;
        this.dexterity = 3;
        this.intelligence = 1;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public void levelUp() {  //adding the right amount of stats to a warrior when leveling up
        this.health += 30;
        this.strength += 5;
        this.dexterity += 2;
        this.intelligence += 1;
    }
}
