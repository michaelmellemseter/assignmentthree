package com.michael.heroes;

import com.michael.items.armors.Armor;
import com.michael.items.weapons.Weapon;

public abstract class Hero {
    String name;
    int xp;
    int level;
    Weapon weapon;
    Armor head;
    Armor body;
    Armor legs;

    //for the extra base bonuses given by an armor
    int extraHeadHealth;
    int extraHeadStrength;
    int extraHeadDexterity;
    int extraHeadIntelligence;
    int extraBodyHealth;
    int extraBodyStrength;
    int extraBodyDexterity;
    int extraBodyIntelligence;
    int extraLegsHealth;
    int extraLegsStrength;
    int extraLegsDexterity;
    int extraLegsIntelligence;

    public Hero(String name) {
        this.name = name;
        this.weapon = null;
        this.head = null;
        this.body = null;
        this.legs = null;
        this.xp = 0;
        this.level = 1;
        this.extraHeadHealth = 0;
        this.extraHeadStrength = 0;
        this.extraHeadDexterity = 0;
        this.extraHeadIntelligence = 0;
        this.extraBodyHealth = 0;
        this.extraBodyStrength = 0;
        this.extraBodyDexterity = 0;
        this.extraBodyIntelligence = 0;
        this.extraLegsHealth = 0;
        this.extraLegsStrength = 0;
        this.extraLegsDexterity = 0;
        this.extraLegsIntelligence = 0;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public int getEffectiveStrength() {
        return getStrength() + extraHeadStrength + extraBodyStrength + extraLegsStrength;
    }

    public int getEffectiveDexterity() {
        return getDexterity() + extraHeadDexterity + extraBodyDexterity + extraLegsDexterity;
    }

    public int getEffectiveIntelligence() {
        return getIntelligence() + extraHeadIntelligence + extraBodyIntelligence + extraLegsIntelligence;
    }

    public void setHead(Armor head) {
        this.head = head;
    }

    public void setBody(Armor body) {
        this.body = body;
    }

    public void setLegs(Armor legs) {
        this.legs = legs;
    }

    public void setExtraHeadHealth(int extraHeadHealth) {
        this.extraHeadHealth = extraHeadHealth;
    }

    public void setExtraHeadStrength(int extraHeadStrength) {
        this.extraHeadStrength = extraHeadStrength;
    }

    public void setExtraHeadDexterity(int extraHeadDexterity) {
        this.extraHeadDexterity = extraHeadDexterity;
    }

    public void setExtraHeadIntelligence(int extraHeadIntelligence) {
        this.extraHeadIntelligence = extraHeadIntelligence;
    }

    public void setExtraBodyHealth(int extraBodyHealth) {
        this.extraBodyHealth = extraBodyHealth;
    }

    public void setExtraBodyStrength(int extraBodyStrength) {
        this.extraBodyStrength = extraBodyStrength;
    }

    public void setExtraBodyDexterity(int extraBodyDexterity) {
        this.extraBodyDexterity = extraBodyDexterity;
    }

    public void setExtraBodyIntelligence(int extraBodyIntelligence) {
        this.extraBodyIntelligence = extraBodyIntelligence;
    }

    public void setExtraLegsHealth(int extraLegsHealth) {
        this.extraLegsHealth = extraLegsHealth;
    }

    public void setExtraLegsStrength(int extraLegsStrength) {
        this.extraLegsStrength = extraLegsStrength;
    }

    public void setExtraLegsDexterity(int extraLegsDexterity) {
        this.extraLegsDexterity = extraLegsDexterity;
    }

    public void setExtraLegsIntelligence(int extraLegsIntelligence) {
        this.extraLegsIntelligence = extraLegsIntelligence;
    }

    public abstract int getHealth();

    public abstract int getDexterity();

    public abstract int getStrength();

    public abstract int getIntelligence();

    public abstract void levelUp();  //giving new stats based on hero type

    public void giveXp(int xp) {  //giving xp and checking if a level up is required
        this.xp += xp;
        if (getLevel() > this.level) {
            for (int i = 0; i < getLevel()-this.level; i++) {  //checking how many levels the player increases
                levelUp();
            }
        }
    }

    public int getLevel() {  //finding the level for the hero based on their xp
        boolean found = false;
        int level = 0;
        double low = 0;
        double high = 100;
        double increase = 100;

        while (!found) {
            if (xp >= low && xp < high) {
                found = true;
            } else {  //setting up to try next level
                low = high;
                increase = increase * 1.10;  //increase next level with 10%
                high += Math.floor(increase);  //high should go from 100 -> 210 -> 331 -> 464...
            }
            level++;
        }
        return level;
    }

    public void equipWeapon(Weapon weapon) {
        if (getLevel() >= weapon.getReqLevel()) {  //checking if the hero has a high enough level
            this.weapon = weapon;
            System.out.println(weapon.getName() + " is equipped");
        } else {
            System.out.println("You don't have high enough level to equip this weapon");
        }
    }

    public void printAttributes() {  //printing all main attributes
        System.out.println("Name: " + name);
        System.out.println("Health: " + getHealth());
        System.out.println("Strength: " + getStrength());
        System.out.println("Dexterity: " + getDexterity());
        System.out.println("Intelligence: " + getIntelligence());
        if (head != null) {
            System.out.println("Head armor: " + head.getName());
        } else {
            System.out.println("Head armor: No head armor");
        }
        if (body != null) {
            System.out.println("Body armor: " + body.getName());
        } else {
            System.out.println("Body armor: No body armor");
        }
        if (legs != null) {
            System.out.println("Legs armor: " + legs.getName());
        } else {
            System.out.println("Legs armor: No legs armor");
        }
        System.out.println("Extra health: " + (extraHeadHealth + extraBodyHealth + extraLegsHealth));
        System.out.println("Extra strength: " + (extraHeadStrength + extraBodyStrength + extraLegsStrength));
        System.out.println("Extra dexterity: " + (extraHeadDexterity + extraBodyDexterity + extraLegsDexterity));
        System.out.println("Extra intelligence: " + (extraHeadIntelligence + extraBodyIntelligence + extraLegsIntelligence));
        if (weapon != null) {
            System.out.println("Weapon: " + weapon.getName());
        } else {
            System.out.println("Weapon: No weapon");
        }
        System.out.println("Level: " + getLevel());
    }
}
