package com.michael.heroes;

public class Ranger extends Hero {
    int health;
    int strength;
    int dexterity;
    int intelligence;

    public Ranger(String name) {  //setting values for attributes to a ranger
        super(name);
        this.health = 120;
        this.strength = 5;
        this.dexterity = 10;
        this.intelligence = 2;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public void levelUp() {  //adding the right amount of stats to a ranger when leveling up
        this.health += 20;
        this.strength += 2;
        this.dexterity += 5;
        this.intelligence += 1;
    }
}
