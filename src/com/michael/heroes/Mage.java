package com.michael.heroes;

public class Mage extends Hero {
    int health;
    int strength;
    int dexterity;
    int intelligence;

    public Mage(String name) {  //setting values for attributes to a mage
        super(name);
        this.health = 100;
        this.strength = 2;
        this.dexterity = 5;
        this.intelligence = 10;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public void levelUp() {  //adding the right amount of stats to a mage when leveling up
        this.health += 15;
        this.strength += 1;
        this.dexterity += 2;
        this.intelligence += 5;
    }
}

