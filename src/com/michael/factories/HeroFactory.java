package com.michael.factories;

import com.michael.heroes.*;

public class HeroFactory {
    public Hero createHero(String type, String name) {  //creating a hero based on type
        if (type == null) {
            return null;
        } else if (type.equalsIgnoreCase("warrior")) {
            return new Warrior(name);
        } else if (type.equalsIgnoreCase("ranger")) {
            return new Ranger(name);
        } else if (type.equalsIgnoreCase("mage")) {
            return new Mage(name);
        } else {
            return null;
        }
    }
}
