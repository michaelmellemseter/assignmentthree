package com.michael.factories;

import com.michael.items.armors.*;
import com.michael.items.armors.equipArmor.EquipArmorStrategy;
import com.michael.items.armors.equipArmor.EquipClothArmor;
import com.michael.items.armors.equipArmor.EquipLeatherArmor;
import com.michael.items.armors.equipArmor.EquipPlateArmor;
import com.michael.items.weapons.*;
import com.michael.items.weapons.dealDamage.MagicWeaponDamage;
import com.michael.items.weapons.dealDamage.MeleeWeaponDamage;
import com.michael.items.weapons.dealDamage.RangedWeaponDamage;
import com.michael.items.weapons.dealDamage.WeaponDamageStrategy;

public class ItemFactory {
    public Weapon createWeapon(String type, String name, int reqLevel) {  //creating a weapon based on type
        //strategies
        WeaponDamageStrategy dealMeleeDamage = new MeleeWeaponDamage();
        WeaponDamageStrategy dealRangedDamage = new RangedWeaponDamage();
        WeaponDamageStrategy dealMagicDamage = new MagicWeaponDamage();

        if (type == null) {
            return null;
        } else if (type.equalsIgnoreCase("melee")) {
            return new MeleeWeapon(name, reqLevel, dealMeleeDamage);
        } else if (type.equalsIgnoreCase("ranged")) {
            return new RangedWeapon(name, reqLevel, dealRangedDamage);
        } else if (type.equalsIgnoreCase("magic")) {
            return new MagicWeapon(name, reqLevel, dealMagicDamage);
        } else {
            return null;
        }
    }

    public Armor createArmor(String type, String name, int reqLevel, int slot) {  //creating an armor based on type
        //strategies
        EquipArmorStrategy equipClothArmor = new EquipClothArmor();
        EquipArmorStrategy equipLeatherArmor = new EquipLeatherArmor();
        EquipArmorStrategy equipPlateArmor = new EquipPlateArmor();

        if (type == null) {
            return null;
        } else if (type.equalsIgnoreCase("cloth")) {
            return new ClothArmor(name, reqLevel, slot, equipClothArmor);
        } else if (type.equalsIgnoreCase("leather")) {
            return new LeatherArmor(name, reqLevel, slot, equipLeatherArmor);
        } else if (type.equalsIgnoreCase("plate")) {
            return new PlateArmor(name, reqLevel, slot, equipPlateArmor);
        } else {
            return null;
        }
    }
}
