package com.michael;

import com.michael.factories.HeroFactory;
import com.michael.factories.ItemFactory;
import com.michael.heroes.*;
import com.michael.items.armors.Armor;
import com.michael.items.weapons.*;

public class Main {

    public static void main(String[] args) {
        //factories
        HeroFactory heroFactory = new HeroFactory();
        ItemFactory itemFactory = new ItemFactory();

        //creating a hero and giving him enough xp to get to level 2
        Hero aage = heroFactory.createHero("mage", "Åge");
        aage.printAttributes();
        System.out.println();
        aage.giveXp(110);
        aage.printAttributes();
        System.out.println();

        //creating a weapon and equipping it to our hero
        Weapon bow = itemFactory.createWeapon("ranged", "Bow and arrow", 2);
        aage.equipWeapon(bow);
        aage.printAttributes();
        System.out.println();

        //creating a weapon and equipping it to our hero
        //should print that Åge is not in high enough level to equip this
        Weapon magicSpell = itemFactory.createWeapon("magic", "Magic spell", 3);
        aage.equipWeapon(magicSpell);
        System.out.println();

        //creating a weapon and switching our hero's weapon
        Weapon axe = itemFactory.createWeapon("melee", "Axe", 1);
        aage.equipWeapon(axe);
        aage.printAttributes();
        System.out.println();

        //creating an armor and equipping it to our hero
        Armor west = itemFactory.createArmor("leather", "West", 2, 2);
        west.equipArmor(aage);
        aage.printAttributes();
        System.out.println();

        //creating an armor and equipping it to our hero on a new slot
        Armor helmet = itemFactory.createArmor("plate", "Helmet", 2, 1);
        helmet.equipArmor(aage);
        aage.printAttributes();
        System.out.println();

        //creating an armor and equipping it to our hero
        //should print that Åge is not in high enough level to equip this
        Armor caps = itemFactory.createArmor("leather", "Caps", 4, 1);
        caps.equipArmor(aage);
        aage.printAttributes();
        System.out.println();

        //letting our hero deal some damage
        aage.getWeapon().dealDamage(aage);
        System.out.println();


        //example from assignment
        System.out.println("Example from assignment:");
        Hero warrior = heroFactory.createHero("warrior", "Warrior");
        warrior.giveXp(1200);
        warrior.printAttributes();
        System.out.println();

        Weapon greatAxe = itemFactory.createWeapon("melee", "Great Axe of the Exiled", 5);
        Armor plateChest = itemFactory.createArmor("plate", "Plate Chest of the Juggernaut", 5, 2);
        warrior.equipWeapon(greatAxe);
        plateChest.equipArmor(warrior);
        System.out.println();

        warrior.printAttributes();
        warrior.getWeapon().dealDamage(warrior);
    }
}
